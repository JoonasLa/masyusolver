import numpy as np
import matplotlib.pyplot as plt
from UI_handler import UI_handler
from Solver import Solver, PuzzleCompleteException, LogicalConflictException
from time import time, sleep
from WebAutomation import WebAutomation
import signal


def signal_handler(signum, frame):
    print("SIGNAALI HÄLYTYS")
    raise TimeoutError


signal.signal(signal.SIGALRM, signal_handler)


# ID: 5,596,959 15x15 hard
# ID: 4,296,525 25x25 hard tosi vaikea koita ratkaista
# 185 to go.

wa = WebAutomation()

def main(size):
    grid = wa.get_grid(size)
    print(grid)
    t1 = time()
    solver = Solver(grid)
    complete = False
    try:
        path = solver.solve_grid()
    except PuzzleCompleteException:
        complete = True
        path = solver.path

    """ except LogicalConflictException:
        complete = False
        path = solver.path
        print("JOKIN MENEE PIELEEEEEN!!!!!") """

    t2 = time()
    print(f"Aikaa kului ratkaisemiseen {(t2-t1)*1000:5.1f} ms")

    if solver.second_order_used:
        print("Käytetty toisen kertaluokan arvaamista")
    if complete:
        signal.alarm(0)
        print("Ratkaistu oikein")
        succ, txt = wa.drag_complete(path)
        # input("Paina enteriä niin syötän tuloksen Hall of Fameen")
        if succ:
            wa.submit_to_HoF()
        return txt

    """else:
        print("Jäi kesken")
        handler.drag_uncomplete(path)"""


if __name__ == "__main__":
    tiedot = list()
    for _ in range(100):
        signal.alarm(10)
        try:
            tiedot.append(main(0))
        except TimeoutError:
            tiedot.append("Aikaraja ylittyi")
    for i, t in enumerate(tiedot):
        print(i, t)


