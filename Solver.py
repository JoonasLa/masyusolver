import numpy as np


class LogicalConflictException(Exception):
    pass


class PuzzleCompleteException(Exception):
    pass


class Solver:
    def __init__(self, grid):
        self.grid = grid
        self.height, self.width = grid.shape
        self.white_beads = np.argwhere(grid == 1)
        self.black_beads = np.argwhere(grid == 2)
        self.tot_num_beads = len(self.white_beads) + len(self.black_beads)
        self.impos_dir = list()
        self.path = list()
        self.something_changed = [True]
        self.guess_depth = 0
        self.guess_path = [set()]
        self.guess_impos = [set()]
        self.second_order_used = False

        for i in range(self.height):
            row = []
            row2 = []
            for j in range(self.width):
                row.append(set())
                row2.append(set())
            self.impos_dir.append(row)
            self.path.append(row2)
        for x in range(self.width):
            self.impos_dir[0][x].add((x, -1))
            self.impos_dir[-1][x].add((x, self.height))
        for y in range(self.height):
            self.impos_dir[y][0].add((-1, y))
            self.impos_dir[y][-1].add((self.width, y))

    def solve_grid(self):
        self.iterate_absolute_sure()
        if self.path_is_complete():
            return self.path
         # return self.path

        anychange = True
        last_loc = (0, 0)
        while anychange:
            anychange = False
            guess_list = self.make_guess_list(last_loc)
            i = 0
            for c1, c2, deny in guess_list:
                i += 1
                if c1 == (3, 3) and c2 == (4, 3):
                    print('debug')
                if c2 in self.path[c1[1]][c1[0]] or c2 in self.impos_dir[c1[1]][c1[0]]:
                    print(f"Arvaus {i:4d}/{len(guess_list):4d} {str(c1):10s} {str(c2):10s} {str(deny):5s} Ei pysty")
                    continue
                try:
                    self.make_guess(c1, c2, deny)
                    self.iterate_absolute_sure()
                    print(f"Arvaus {i:4d}/{len(guess_list):4d} {str(c1):10s} {str(c2):10s} {str(deny):5s} Ei konfliktia")
                    self.cancel_guess()
                except LogicalConflictException:
                    print(f"Arvaus {i:4d}/{len(guess_list):4d} {str(c1):10s} {str(c2):10s} {str(deny):5s} Tapahtui konflikti")
                    self.cancel_guess()
                    last_loc = c1
                    anychange = True
                    if deny:
                        self.add_to_path(c1, c2)
                    else:
                        self.deny_path(c1, c2)
                    self.iterate_absolute_sure()

            if not anychange:
                print("JOUDUTAAN OTTAMAAN ISOMPI VAIHDE KÄYTTÖÖN!!!!")
                # return self.path
                self.second_order_used = True
                i1 = 0
                for c1, c2, deny in guess_list:
                    self.make_guess(c1, c2, deny)
                    try:
                        guess_list2 = self.make_guess_list(c1)[::-1]
                        i2 = 0
                        for c3, c4, deny2 in guess_list2:
                            if deny2:
                                if c4 in self.path[c3[1]][c3[0]]:
                                    continue
                            else:
                                if c4 in self.impos_dir[c3[1]][c3[0]]:
                                    continue
                            try:
                                self.make_guess(c3, c4, deny2)
                                self.iterate_absolute_sure()
                                self.cancel_guess()
                                print(f"2. asteen arvaus {i1:4d}/{len(guess_list):4d} {str(c1):10s} {str(c2):10s} {str(deny):5s} {i2:4d}/{len(guess_list2):4d} {str(c3):10s} {str(c4):10s} {str(deny2):5s} Ei konfliktia")
                            except LogicalConflictException:
                                self.cancel_guess()
                                if deny2:
                                    self.add_to_path(c3, c4)
                                else:
                                    self.deny_path(c3, c4)
                                print(f"2. asteen arvaus {i1:4d}/{len(guess_list):4d} {str(c1):10s} {str(c2):10s} {str(deny):5s} {i2:4d}/{len(guess_list2):4d} {str(c3):10s} {str(c4):10s} {str(deny2):5s} Kyllä konfliktia")
                                self.iterate_absolute_sure()
                            i2 += 1
                        self.cancel_guess()
                        i1 += 1
                    except LogicalConflictException:
                        self.cancel_guess()
                        if deny:
                            self.add_to_path(c1, c2)
                        else:
                            self.deny_path(c1, c2)
                        anychange = True
                        break
        return self.path

    def make_guess(self, c1, c2, deny=False):
        self.guess_depth += 1
        assert self.guess_depth < 4, "Liian syvällä"
        self.guess_path.append(set())
        self.guess_impos.append(set())
        self.something_changed.append(True)
        assert self.guess_depth == len(self.guess_path) - 1 == len(self.guess_impos) - 1
        if deny:
            self.deny_path(c1, c2)
        else:
            self.add_to_path(c1, c2)

    def cancel_guess(self):
        self.guess_depth -= 1
        self.something_changed.pop()
        path_remove = self.guess_path.pop()
        for c1, c2 in path_remove:
            self.path[c1[1]][c1[0]].remove(c2)
            self.path[c2[1]][c2[0]].remove(c1)
        impos_remove = self.guess_impos.pop()
        for c1, c2 in impos_remove:
            self.impos_dir[c1[1]][c1[0]].remove(c2)
            if 0 <= c2[0] < self.height and 0 <= c2[1] < self.width:
                self.impos_dir[c2[1]][c2[0]].remove(c1)
        assert self.guess_depth == len(self.guess_path) - 1 == len(self.guess_impos) - 1

    def make_guess_list(self, near_to_loc):
        path_guess = []
        deny_guess = []
        for y in range(self.height):
            for x in range(self.width):
                c1 = (x, y)
                for d in ((1, 0), (0, 1)):
                    xx, yy = x + d[0], y + d[1]
                    c2 = (xx, yy)
                    if c2 not in self.impos_dir[y][x] and c2 not in self.path[y][x]:
                        cond1 = self.grid[c1[1]][c1[0]] == 1 or self.grid[c2[1]][c2[0]] == 1
                        cond2 = self.grid[c1[1]][c1[0]] == 2 or self.grid[c2[1]][c2[0]] == 2
                        cond3 = len(self.path[c1[1]][c1[0]]) == 1 or len(self.path[c2[1]][c2[0]]) == 1
                        cond4 = True
                        if cond2 or cond3:
                            path_guess.append((c1, c2, False))
                            deny_guess.append((c1, c2, True))

        guess_list = path_guess  #  + deny_guess
        # np.random.seed(3)
        np.random.shuffle(guess_list)
        # guess_list = sorted(guess_list, key=lambda x:abs(x[0][0]-near_to_loc[0]) + abs(x[0][1] - near_to_loc[1]))
        return guess_list

    def iterate_absolute_sure(self):
        self.something_changed[self.guess_depth] = True
        while self.something_changed[self.guess_depth]:
            self.something_changed[self.guess_depth] = False
            self.solve_white_beads()
            self.solve_black_beads()
            self.solve_dead_end()
            self.block_complete_dead_ends()
            self.solve_non_looping()
            if self.something_changed[self.guess_depth]:
                continue
            self.all_paths_can_be_connected()
            self.count_even_number_of_open_ends()
            if self.path_is_complete() or not self.something_changed[self.guess_depth]:
                break

    def solve_white_beads(self):
        for y, x in self.white_beads:
            dir = None
            impdir = None
            dir1 = ((x, y + 1), (x, y - 1))
            dir2 = ((x - 1, y), (x + 1, y))
            dir3 = ((x, y+2), (x, y - 2))
            dir4 = ((x - 2, y), (x + 2, y))

            if len(self.impos_dir[y][x]) != 0:
                imp = self.impos_dir[y][x]
                if dir1[0] not in imp and dir1[1] not in imp:
                    dir = dir1
                    impdir = dir2
                else:
                    dir = dir2
                    impdir = dir1

            elif len(self.path[y][x]) > 0:
                if dir1[0] in self.path[y][x] or dir1[1] in self.path[y][x]:
                    dir = dir1
                    impdir = dir2
                else:
                    dir = dir2
                    impdir = dir1
            if dir is None:     # Toimii koska reunassa aina dir määritetty
                if self.grid[dir1[0][1], dir1[0][0]] == 1 and self.grid[dir1[1][1], dir1[1][0]] == 1:
                    impdir = dir1
                    dir = dir2
                elif self.grid[dir2[0][1], dir2[0][0]] == 1 and self.grid[dir2[1][1], dir2[1][0]] == 1:
                    impdir = dir2
                    dir = dir1
                elif dir3[0] in self.path[dir1[0][1]][dir1[0][0]] and dir3[1] in self.path[dir1[1][1]][dir1[1][0]]:
                    impdir = dir1
                    dir = dir2
                elif dir4[0] in self.path[dir2[0][1]][dir2[0][0]] and dir4[1] in self.path[dir2[1][1]][dir2[1][0]]:
                    impdir = dir2
                    dir = dir1
            if dir is not None:
                self.add_to_path((x, y), dir[0])
                self.add_to_path((x, y), dir[1])
                self.deny_path((x, y), impdir[0])
                self.deny_path((x, y), impdir[1])
                d = (dir[0][0]-x, dir[0][1] - y)
                dir2 = dir[0][0] + d[0], dir[0][1] + d[1]
                dir3 = dir[1][0] - d[0], dir[1][1] - d[1]
                if dir2 in self.path[dir[0][1]][dir[0][0]]:
                    self.deny_path(dir[1], dir3)
                if dir3 in self.path[dir[1][1]][dir[1][0]]:
                    self.deny_path(dir[0], dir2)

    def solve_black_beads(self):
        for y, x in self.black_beads:
            impdir = set()
            for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                xx, yy = x, y
                for _ in range(2):
                    xxx, yyy = xx + d[0], yy + d[1]
                    if (xxx, yyy) in self.impos_dir[yy][xx]:
                        impdir.add(d)
                        break
                    xx, yy = xxx, yyy
                else:
                    xx, yy = x + d[0], y + d[1]
                    if self.grid[yy][xx] == 2:
                        impdir.add(d)
                    if (xx, yy) in self.path[y][x]:
                        impdir.add((-d[0], -d[1]))
                    if len(self.path[yy][xx]) > 0:
                        xxx, yyy = xx + d[0], yy + d[1]
                        if not((xxx, yyy) in self.path[yy][xx] or (x, y) in self.path[yy][xx]):
                            impdir.add(d)

            for d in impdir:
                dir = (x + d[0], y + d[1])
                dir2 = (x - d[0], y - d[1])
                dir3 = (x - 2 * d[0], y - 2 * d[1])
                self.deny_path((x, y), dir)
                self.add_to_path((x, y), dir2)
                self.add_to_path(dir2, dir3)

    def solve_dead_end(self):
        for y in range(self.height):
            for x in range(self.width):
                if len(self.path[y][x]) == 1 and len(self.impos_dir[y][x]) == 2:
                    for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                        xx, yy = x + d[0], y + d[1]
                        if (xx, yy) not in self.path[y][x] and (xx, yy) not in self.impos_dir[y][x]:
                            self.add_to_path((x, y), (xx, yy))
                            break
                    else:
                        raise LogicalConflictException()


    def block_complete_dead_ends(self):
        for y in range(self.height):
            for x in range(self.width):
                if len(self.impos_dir[y][x]) == 3:
                    for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                        xx, yy = x + d[0], y + d[1]
                        if (xx, yy) not in self.impos_dir[y][x]:
                            self.deny_path((x, y), (xx, yy))
                            break

    def solve_non_looping(self):
        for y in range(self.height):
            for x in range(self.width):
                if len(self.path[y][x]) == 1:
                    x2, y2, numbeads = self.other_end_of_path(x, y)
                    if abs(x - x2) + abs(y - y2) == 1 and numbeads < self.tot_num_beads and (x2, y2) not in self.path[y][x]:
                        self.deny_path((x, y), (x2, y2))

    def other_end_of_path(self, x, y):
        lx, ly = x, y
        x, y = self.path[y][x].pop()
        self.path[ly][lx].add((x, y))
        numbeads = 0
        if self.grid[ly][lx] != 0:
            numbeads += 1

        while len(self.path[y][x]) == 2:
            if self.grid[y][x] != 0:
                numbeads += 1
            for xx, yy in self.path[y][x]:
                if (xx, yy) != (lx, ly):
                    lx, ly = x, y
                    x, y = xx, yy
                    break
            else:
                assert False, "Virhe"
        if self.grid[y][x] != 0:
            numbeads += 1
        return x, y, numbeads

    def all_paths_can_be_connected(self):
        for y in range(self.height):
            for x in range(self.width):
                if len(self.path[y][x]) == 1:
                    othend, ac2 = self.find_other_ends(x, y)
                    if len(othend) == 0:
                        raise LogicalConflictException()
                    if len(othend) == 1:
                        xx, yy = othend[0]
                        while (xx, yy) != (x, y):
                            nextc = ac2[(xx, yy)]
                            self.make_guess((xx, yy), nextc, deny=True)
                            o2, _ = self.find_other_ends(x, y)
                            self.cancel_guess()
                            if len(o2) == 0:
                                self.add_to_path((xx, yy), nextc)
                            xx, yy = nextc

    def find_other_ends(self, x, y):
        accessible = [(x, y)]
        ac2 = {(x, y): None}
        i = 0
        othend = []
        xoth, yoth, numbeads = self.other_end_of_path(x, y)

        while i < len(accessible):
            xx, yy = accessible[i]
            for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                xxx, yyy = xx + d[0], yy + d[1]
                if (xxx, yyy) not in self.impos_dir[yy][xx] and (xxx, yyy) not in ac2:
                    if len(self.path[yyy][xxx]) == 0:
                        accessible.append((xxx, yyy))
                    ac2[(xxx, yyy)] = (xx, yy)
                    if len(self.path[yyy][xxx]) == 1:
                        othend.append((xxx, yyy))
                        if len(othend) > 2:
                            return othend, ac2
            i += 1
        if len(othend) == 1:
            xx, yy = othend[0]
            if (xx, yy) == (xoth, yoth):
                if numbeads != self.tot_num_beads:
                    othnum = self.available_num_beads_in_area((x, y))
                    if othnum + numbeads != self.tot_num_beads:
                        return [], ac2
        return othend, ac2

    def available_num_beads_in_area(self, c1):
        checked = {c1}
        ch2 = [c1]
        i = 0
        numbeads = 0
        while i < len(ch2):
            xx, yy = ch2[i]
            for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                xxx, yyy = xx + d[0], yy + d[1]
                if (xxx, yyy) not in self.impos_dir[yy][xx] and len(self.path[yyy][xxx]) == 0 and (xxx, yyy) not in checked:
                    checked.add((xxx, yyy))
                    ch2.append((xxx, yyy))
                    if self.grid[yyy][xxx] != 0:
                        numbeads += 1
            i += 1
        return numbeads

    def count_even_number_of_open_ends(self):
        checked = set()
        for y in range(self.height):
            for x in range(self.width):
                if (x, y) in checked:
                    continue
                if len(self.path[y][x]) == 1:
                    ch2 = [(x, y)]
                    checked.add((x, y))
                    i = 0
                    numends = 1
                    while i < len(ch2):
                        xx, yy = ch2[i]
                        for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                            xxx, yyy = xx + d[0], yy + d[1]
                            if (xxx, yyy) not in self.impos_dir[yy][xx] and (xxx, yyy) not in self.path[yy][xx] and (xxx, yyy) not in checked:
                                checked.add((xxx, yyy))
                                ch2.append((xxx, yyy))
                                if len(self.path[yyy][xxx]) == 1:
                                    numends += 1
                        i += 1
                    if numends % 2 != 0:
                        raise LogicalConflictException()



    def path_is_complete(self):
        sx, sy = None, None
        for y in range(self.height):
            for x in range(self.width):
                if len(self.path[y][x]) > 0:
                    sx, sy = x, y
                    break
            else:
                continue
            break
        if sx is None:
            return False
        x, y = sx, sy
        coor_checked = set()
        coor_checked.add((x, y))
        num_beads = 0
        if self.grid[y, x] != 0:
            num_beads += 1

        x2, y2 = self.path[y][x].pop()
        self.path[y][x].add((x2, y2))
        x, y = x2, y2
        while (x, y) not in coor_checked:
            if self.grid[y, x] != 0:
                num_beads += 1
            if len(self.path[y][x]) != 2:
                return False
            coor_checked.add((x, y))
            for x2, y2 in self.path[y][x]:
                if (x2, y2) not in coor_checked:
                    x, y = x2, y2
                    break
            else:
                break
        if num_beads == len(self.black_beads) + len(self.white_beads):
            raise PuzzleCompleteException
        return num_beads == len(self.black_beads) + len(self.white_beads)

    def add_to_path(self, c1, c2):
        self.coord_inside(c1)
        self.coord_inside(c2)
        if c2 in self.impos_dir[c1[1]][c1[0]]:
            raise LogicalConflictException("Virhe")
        if c2 not in self.path[c1[1]][c1[0]]:
            if len(self.path[c1[1]][c1[0]]) == 1:
                x2, y2, numbeads = self.other_end_of_path(c1[0], c1[1])
                if (x2, y2) == c2 and numbeads < self.tot_num_beads:
                    raise LogicalConflictException("Virhe")
            self.something_changed[self.guess_depth] = True
            self.guess_path[self.guess_depth].add((c1, c2))
            self.path[c1[1]][c1[0]].add(c2)
            self.path[c2[1]][c2[0]].add(c1)

        if len(self.path[c1[1]][c1[0]]) == 2:
            for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                xx, yy = c1[0] + d[0], c1[1] + d[1]
                if (xx, yy) not in self.path[c1[1]][c1[0]]:
                    self.deny_path(c1, (xx, yy))
        if len(self.path[c2[1]][c2[0]]) == 2:
            for d in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                xx, yy = c2[0] + d[0], c2[1] + d[1]
                if (xx, yy) not in self.path[c2[1]][c2[0]]:
                    self.deny_path(c2, (xx, yy))

    def deny_path(self, c1, c2):
        self.coord_inside(c1)
        if c2 in self.path[c1[1]][c1[0]]:
            raise LogicalConflictException("Virhe")
        if c2 not in self.impos_dir[c1[1]][c1[0]]:
            self.something_changed[self.guess_depth] = True
            self.guess_impos[self.guess_depth].add((c1, c2))
        self.impos_dir[c1[1]][c1[0]].add(c2)
        if 0 <= c2[1] < self.height and 0 <= c2[0] < self.width:
            self.impos_dir[c2[1]][c2[0]].add(c1)

    def coord_inside(self, c):
        assert 0 <= c[1] < self.height and 0 <= c[0] < self.width, "Ei ole"

    def print_path(self):
        print("#" * 20)
        for y in range(self.height):
            for x in range(self.width):
                print(self.path[y][x], "|", end="")
            print()
        print("#"*20)

def main():
    grid = np.array([[0, 0, 1, 0, 1, 0],
 [0, 0, 2, 0, 0, 1],
 [0, 1, 1, 0, 0, 0],
 [0, 0, 2, 1, 1, 2],
 [1, 0, 0, 1, 0, 1],
 [0, 1, 0, 0, 0, 0]])
    solver = Solver(grid)
    solver.solve_grid()


if __name__ == "__main__":
    main()
