import numpy as np
import matplotlib.pyplot as plt
import pyautogui as pag
import pyperclip
import pyscreenshot
from time import sleep

SCREEN_MODE = 0     # 0 = Kotona kahdella näytöllä, 1 asunnolla kolmella näytöllä, 2 pelkkä läppäri
ADDRESS = "https://www.puzzle-masyu.com/?size="
global PYCHARM_HIDDEN
PYCHARM_HIDDEN = False
pag.FAILSAFE = True

class UI_handler:
    def __init__(self, size: int):
        pag.FAILSAFE = True
        self.size = size
        self.blue_corners = None
        self.width_coord = None
        self.height_coord = None
        self.screen_scrolled = False

        if SCREEN_MODE == 0:
            self.address_bar_location = (2500, 500)
            self.puzzle_location = (2950, 800)
            self.new_puzzle_loc = (3150, 850)
            self.start_over_loc = ((2900, 850), (3000, 925))
            self.laptop_upscreen_pixel_loc = 410
        elif SCREEN_MODE == 1:
            self.address_bar_location = (1800, 85)
            self.puzzle_location = (2400, 400)
            self.new_puzzle_loc = (2520, 430)
            self.start_over_loc = ((2270, 430), (2350, 510))
            self.laptop_upscreen_pixel_loc = 0
        elif SCREEN_MODE == 2:
            global PYCHARM_HIDDEN
            if not PYCHARM_HIDDEN:
                pag.click(1865, 40)
                sleep(0.5)
                PYCHARM_HIDDEN = True
            self.address_bar_location = (550, 85)
            self.puzzle_location = (1100, 400)
            self.new_puzzle_loc = (1240, 430)
            self.start_over_loc = ((990, 430), (1070, 510))
            self.laptop_upscreen_pixel_loc = 0


    def get_puzzle_input(self, new_tab=True, reset_puzzle=True):
        if new_tab:
            im = self.open_new_tab(reset_puzzle)
        else:
            im = self.cut_puzzle_from_screenshot(np.array(pyscreenshot.grab()))
        while True:
            try:
                return self.read_puzzle(im)
            except AssertionError as e:
                print(e)
            except ValueError as e:
                print(e)
            except IndexError as e:
                print(e)
            im = self.cut_puzzle_from_screenshot(np.array(pyscreenshot.grab()))

    def scroll_screen_up(self):
        assert self.screen_scrolled
        pag.vscroll(5)
        self.screen_scrolled = False
        sleep(1.0)

    def scroll_screen_down(self):
        assert not self.screen_scrolled
        pag.vscroll(-5)
        self.screen_scrolled = True
        sleep(1.0)

    def open_new_tab(self, reset_puzzle):
        pag.keyDown('ctrl')
        pag.click(*self.address_bar_location)
        pag.press('t')
        pag.click(*self.address_bar_location)
        pag.press('a')
        pag.press('x')
        pyperclip.copy(ADDRESS + str(self.size))
        pag.press('v')
        pag.keyUp('ctrl')
        pag.press('enter')
        pag.moveRel(0, 100)
        if reset_puzzle:
            sleep(2.0)
            pag.vscroll(-30)
            sleep(0.5)

            if 12 < self.size < 16:
                pag.click(*self.start_over_loc[0])
                sleep(0.5)
                pag.click(*self.start_over_loc[1])
            else:
                pag.click(*self.new_puzzle_loc)
        if self.size > 12:
            sleep(2.0)
            if 12 < self.size < 16:
                pag.press('f11')
                sleep(1.0)
            pag.moveRel(0, 100)
            pag.vscroll(-2)
            sleep(1.0)

        if self.size < 14 or self.size > 15:
            while True:
                im = np.array(pyscreenshot.grab())
                try:
                    return self.cut_puzzle_from_screenshot(im)
                except IndexError:
                    print("Ei ladannut vielä")
        else:
            im1 = np.array(pyscreenshot.grab())
            sleep(1.0)
            self.scroll_screen_down()
            im2 = np.array(pyscreenshot.grab())
            im3 = np.concatenate((im1[:self.laptop_upscreen_pixel_loc + 375, :, :], im2[self.laptop_upscreen_pixel_loc + 90:, :, :]), axis=0)
            self.scroll_screen_up()
            return self.cut_puzzle_from_screenshot(im3)

    def cut_puzzle_from_screenshot(self, im):
        x, y = self.puzzle_location
        while im[y, x, 0] != 51:
            x -= 1
        xleft = x
        x, y = self.puzzle_location
        while im[y, x, 0] != 51:
            x += 1
        xright = x
        x, y = self.puzzle_location
        while im[y, x, 0] != 51:
            y -= 1
        yup = y
        x, y = self.puzzle_location
        while im[y, x, 0] != 51:
            y += 1
        ydown = y
        self.blue_corners = (xleft, xright, yup, ydown)
        return im[yup+1:ydown, xleft+1:xright, :]

    def read_puzzle(self, im):
        width, height = [], []
        for x in range(len(im[15, :, 0])):
            if im[15, x, 0] == 0:
                width.append(x)
        for y in range(len(im[:, 15, 0])):
            if im[y, 15, 0] == 0:
                height.append(y)

        self.height_coord = height
        self.width_coord = width
        print(width, height)
        assert width == height, "WIDTH != HEIGHT"
        for i in range(len(width)-1):
            assert width[i+1] - width[i] == 31, "WRONG BOX SIZE"
            assert height[i+1] - height[i] == 31, "WRONG BOX SIZE"

        grid = np.zeros((len(height)-1, len(width)-1), dtype=int)
        for y in range(len(width) - 1):
            for x in range(len(height) - 1):
                box = im[height[y]:height[y+1]+1, width[x]:width[x+1]+1, :]
                if box[15, 15, 0] == 0:
                    type = 2
                elif 46 < box[15, 15, 0] < 50:
                    type = 0
                elif box[15, 15, 0] == 255:
                    type = 1
                else:
                    plt.imshow(box)
                    plt.show()
                    plt.imshow(im)
                    plt.show()
                    assert False, "BOX READ WRONG"

                grid[y, x] = type
        return grid

    def puzzle_coord_to_screen_coord(self, x, y):
        sx = self.blue_corners[0] + self.width_coord[x] + 15
        sy = self.blue_corners[2] + self.height_coord[y] + 15
        if self.screen_scrolled:
            sy -= 285
        return sx, sy

    def drag_uncomplete(self, path):
        pag.PAUSE = 0.05
        for y in range(len(path)):
            for x in range(len(path[0])):
                while len(path[y][x]) > 0:
                    xx, yy = path[y][x].pop()
                    path[yy][xx].remove((x, y))
                    sx1, sy1 = self.puzzle_coord_to_screen_coord(x, y)
                    sx2, sy2 = self.puzzle_coord_to_screen_coord(xx, yy)
                    if not pag.onScreen(sx1, sy1) or not pag.onScreen(sx2, sy2):
                        if self.screen_scrolled:
                            self.scroll_screen_up()
                        else:
                            self.scroll_screen_down()
                        sx1, sy1 = self.puzzle_coord_to_screen_coord(x, y)
                        sx2, sy2 = self.puzzle_coord_to_screen_coord(xx, yy)
                        assert pag.onScreen(sx1, sy1) and pag.onScreen(sx2, sy2), "Nyt ei täsmää"
                    pag.mouseDown(sx1, sy1)
                    pag.mouseUp(sx2, sy2)
        if len(path) > 25:
            pag.press('f11')


    def drag_complete(self, path):
        if len(path) > 30:
            self.drag_uncomplete(path)
            pag.press('enter')
            return
        sx, sy = None, None
        for y in range(len(path)):
            for x in range(len(path[0])):
                if len(path[y][x]) == 2:
                    sx, sy = x, y
                    break
        x, y = sx, sy
        stx, sty = x, y
        sx, sy = self.puzzle_coord_to_screen_coord(x, y)
        pag.mouseDown(sx, sy)
        pag.PAUSE = 0.04
        if len(path) > 25:
            pag.PAUSE = 0.06

        x2, y2 = path[y][x].pop()
        sx2, sy2 = self.puzzle_coord_to_screen_coord(x2, y2)
        pag.moveTo(sx2, sy2)
        lx, ly = x, y
        x, y = x2, y2
        i = 0
        while True:
            for x2, y2 in path[y][x]:
                if (x2, y2) != (lx, ly):
                    sx2, sy2 = self.puzzle_coord_to_screen_coord(x2, y2)
                    pag.moveTo(sx2, sy2)
                    lx, ly = x, y
                    x, y = x2, y2
                    break
            if (x, y) == (stx, sty):
                break
            i += 1
            if i > len(path) * len(path[0]) + 1:
                input("Pysäytetty koska liian pitkä iteraatio")
                break
        pag.mouseUp()
        pag.press('enter')
        if len(path) > 25:
            pag.press('f11')


def main():
    hand = UI_handler(16)
    im = pyscreenshot.grab()
    plt.imshow(im)
    plt.show()
    pag.moveTo(1810, 1010)
    hand.scroll_screen_down()
    pag.moveTo(1810, 1010-285)


if __name__ == "__main__":
    main()