import string
import selenium.common.exceptions
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time
import os
import numpy as np


class WebAutomation:
    def __init__(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        # fireFoxOptions.headless = True
        self.driver = webdriver.Firefox(firefox_options=fireFoxOptions)
        self.game = None
        self.time_start = None
        self.time_stop = None
        self.driver.maximize_window()
        self.driver.get("https://www.puzzle-masyu.com/?size=0")
        while True:
            try:
                self.driver.find_element_by_class_name("css-1hy2vtq").click()
                break
            except (selenium.common.exceptions.NoSuchElementException, selenium.common.exceptions.StaleElementReferenceException):
                print("Ei vielä")

    def get_grid(self, size):
        self.driver.get("https://www.puzzle-masyu.com/?size=" + str(size))
        if not 13 <= size <= 15:
            self.driver.find_element_by_id("btnNew").click()
        self.time_start = time.time()
        robot = self.driver.find_element_by_id("robot")
        self.driver.execute_script("arguments[0].value='1'", robot)
        self.game = self.driver.find_element_by_id("game")
        return get_grid(self.driver.page_source)

    def drag_complete(self, solution):
        print(solution)
        actions = ActionChains(self.driver)
        prelist = list()


        start = None
        mlist = list()
        for i in range(len(solution)**2):
            x, y = i % len(solution), i // len(solution)
            if len(solution[y][x]) > 0:
                start = (x, y)
                break
            prelist.append(Keys.ARROW_RIGHT)
            if x == len(solution) - 1:
                prelist = [Keys.ARROW_DOWN]

        actions.send_keys([Keys.ARROW_RIGHT, Keys.ARROW_LEFT])
        actions.send_keys(prelist)
        actions.key_down(Keys.CONTROL)
        prev = None
        cur = start
        while prev is None or cur != start:
            x, y = cur
            for nex in solution[y][x]:
                if nex == prev:
                    continue
                nx, ny = nex
                if nx > x:
                    mlist.append(Keys.ARROW_RIGHT)
                elif nx < x:
                    mlist.append(Keys.ARROW_LEFT)
                elif ny > y:
                    mlist.append(Keys.ARROW_DOWN)
                elif ny < y:
                    mlist.append(Keys.ARROW_UP)
                prev = cur
                cur = nex
                break
        actions.send_keys(mlist)
        actions.key_up(Keys.CONTROL)
        actions.perform()
        self.time_stop = time.time()
        self.driver.find_element_by_id("btnReady").click()

        try:
            txt = self.driver.find_element_by_class_name("succ").text + f" ja oma {self.time_stop - self.time_start:5.3f} s"
            print(txt)
            return True, txt
        except selenium.common.exceptions.NoSuchElementException:
            print("Virhe, ei voi tallentaa")
            return False, None

    def submit_to_HoF(self):
        self.driver.find_element_by_id("btnHallSubmit").click()
        self.driver.find_element_by_name("email").send_keys("joonas.latukka@gmail.com")
        btns = self.driver.find_elements_by_class_name("button")
        for but in btns:
            if but.get_attribute("value") == "  Submit your score  ":
                but.click()
                break


def get_grid(source):
    t1 =source.split("var task = '")[1]
    task = t1.split("'", 1)[0]
    height = int(t1.split("puzzleWidth: ", 1)[1].split(",", 1)[0])
    grid = np.zeros((height, height), dtype=int)
    i = 0
    for l in task:
        x, y = i % height, i // height
        if l == "B":
            grid[y, x] = 2
            i += 1
        elif l == "W":
            grid[y, x] = 1
            i += 1
        else:
            for ll in string.ascii_lowercase:
                i += 1
                if ll == l:
                    break
    return grid


if __name__ == "__main__":
    main()
